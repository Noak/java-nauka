/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.ArrayList;

/**
 *
 * @author Damian&Lukasz
 */
public class Zwierzatko {
    private  String pseudonim;
    private  double wiek;
    private  String gatunek;

    
    public Zwierzatko(String pseudonim,double wiek,String gatunek){
        this.pseudonim=pseudonim;
        this.wiek=wiek;
        this.gatunek=gatunek;
        }
    
    public  String getPseudonim()
    {
        return pseudonim;
    }
    
     public double getWiek()
    {
        return wiek;
    }
      public String getGatunek()
    {
        return gatunek;
    }
      
      public String toString(){
     return "Imie: "+this.getPseudonim()+" "+
             "Wiek: "+this.getWiek()+" "+"Gatunek: "+this.getGatunek();
}


}





